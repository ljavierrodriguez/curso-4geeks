var http = new XMLHttpRequest();

var $text = document.getElementById("text");
var $html = document.getElementById("html");

// GET, POST, PUT, PATCH, DELETE
/*http.open('GET', 'procesar.php', true);
http.onreadystatechange = procesarP;
http.send("nombre=luis"); // Formato Clave=Valor
//http.send("nombre:luis"); // Formato JSON

// readyState 0. UNSET, 1. OPENED, 2. HEADERS_RECEIVED, 3. LOADING, 4. DONE
// status 200 == 0k ... 404 == Page Not Found

function procesarP(){
    if(http.readyState === 4 && http.status === 200){
        console.log("Pagina Encontrada");
        document.write(http.responseText);
    }
}
*/

$text.addEventListener("click", function(){
    peticionTxt('texto.txt');
});
$html.addEventListener("click", function(){
    peticionHTML('saludo.html');
});

function peticionTxt(url){
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 200){
            console.log("Encontrad0");
            document.getElementById('salida').innerHTML = http.responseText;
        }
    }

    http.open('GET', url, true);
    http.send();
    document.getElementById('salida').innerHTML = 'cargando...';
}

function peticionHTML(url){
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 200){
            console.log("Encontrad0");
            document.getElementById('salida').innerHTML = http.responseText;
        }
    }

    http.open('GET', url, true);
    http.send();
    document.getElementById('salida').innerHTML = 'cargando...';
}

var $json = document.getElementById('json');
$json.addEventListener("click", function(){
    console.log("Click");
    enviarHeroe('{name:"Clark Kent"}');
});

function enviarHeroe(heroe){
    var http = new XMLHttpRequest();
    http.open('POST', 'procesar.php', true);
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 200){
            console.log("Encontrado");
            console.log(http.responseText);
        }
    }
    http.send(heroe);
}