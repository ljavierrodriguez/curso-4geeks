var quiz = {
    "nombre": "Quiz de Nombres de Super Heroes",
    "descripcion": "Cuantos nombres de heroes conoces?",
    "pregunta": "Cual es el nombre real de: ",
    "preguntas": [
        {"pregunta": "Superman", "respuesta": "Clark Kent"},
        {"pregunta": "Spiderman", "respuesta": "Peter Parker"},
        {"pregunta": "Batman", "respuesta": "Bruce Wayne"},
        {"pregunta": "Wonderwoman", "respuesta": "Diana Prince"},
    ],
}

var score = 0;

play(quiz);

function play(quiz){
    for(var i = 0, pregunta, respuesta, max=quiz.preguntas.length; i < max; i++ ){
        pregunta = quiz.preguntas[i].pregunta;
        respuesta = preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();

    function preguntar(pregunta){
        return prompt(quiz.pregunta + pregunta);
    }
    function verificar(respuesta){
        if(respuesta === quiz.preguntas[i].respuesta){
            alert("Respuesta Correcta");
            score += 10;
        }else{
            alert("Respuesta Incorrecta");
            score -= 5;
        }
    }
    function gameOver(){
        alert("Game Over, Su puntuacion fue " + score + " puntos");
    }
}