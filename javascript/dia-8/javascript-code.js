// Comentarios

// Comentario Corto

/*
Comentario Largo o Multiples Lineas
*/

// Gramatica de Javascript

a = "Hola Mundo"
alert(a)

// ó

a = "Hola Mundo";
alert(a);

// Bloques de Codigo
{
    // Este bloque contiene dos sentencias
    var a = "Hola";
    alert(a);
}

// Tipos de Datos
/*
string
number
Boolean
undefined
null
*/
let nombre = 'Pedro'; typeof nombre; // string
let edad = 5; typeof edad; // number
let soltero = true; typeof soltero; // boolean
typeof apellido; // undefined
let flag = null; typeof flag; // object
let estudiante = { nombre: 'Luis', apellido: 'Rodriguez'}; typeof estudiante; // object


// Escape de Caracteres 
let apellido = "D\'ambrosio"; // \' escapando comilla simple
let mensaje = " cita: \"Esto es una cita\" "; // \" escapando comillas dobles
let mensaje2 = "Esto es mi primer linea \n y esto va a la segunda linea"; // \n nueva linea
let mensaje3 = "Esto es otra prueba \r"; // \r Retorno de carga
let mensaje4 = "Esto esta separado por \t tabulacion" // \t Tabulado

// Reglas para definir una variable

$nombre = "luis"; // Puedo comenzar con $
_nombre = "Pedro"; // Puedo comenzar con _
nombre = "Luis"; // Puedo definirla solo letras
primer_nombre = "Luis"; // Puedo Separarla con _
primerNombre = "Luis"; // Puedo definicarla usando camelCase
nombre1 = "Luis"; // Puedo incluir numeros pero no comenzar con numero

// Todas las Variables son "case sensitive", NOMBRE no es igual a Nombre o NomBre

// Palabras reservadas

/*

abstract, boolean, break, byte, case, catch, char, class, const,
continue, debugger, default, delete, do, double, else, enum,
export, extends, false, final, finally, float, for, function,
goto, if, implements, import, in instanceof, int, inteface,
long, native, new, null, package, private, protected, public,
return, short, static, super, switch, synchronized, this, throw,
throws, transient, true, try, typeof, var, volatile, void, while,
with

*/

// Asignaciones

var a = "Hola";

let a, b, c;

let a = 1, b , c=5;

b = 7;
