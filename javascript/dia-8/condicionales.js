// Sentencia IF, IF ELSE, IF ELSE IF ELSE

var a = 5;
var b = 3;
var c = 8;

if(a > b || b != 0){
    // sentencias a ejecutar
}

if(a > b){
    console.log("a es mayor que b")
}else{
    console.log("b es mayor que a")
}

if(a > b && a > c){
    console.log("A es el mayor")
}else if(b > a && b > c ){
    console.log("B es el mayor")
}else{
    console.log("C es el mayor")
}

var edad = 13;
if(edad < 18){
    console.log("Lo siento no puede ver este contenido")
}

// Operador Ternario

n = 5;
// condicion  IF Verdadero      ELSE   Falso 
n%2===0        ? console.log("Es par") : console.log("Es impar");

flag = true;
flag ? alert("verdadero") : alert("falso");

edad = 21;
edad >= 18 ? console.log("es mayor") : console.log("es menor");

// Sentencia Switch 
var opt = 4;

if(opt === 1){
    console.log("Opcion del Menu 1");
}else if(opt === 2){
    console.log("Opcion del Menu 2");
}else if(opt === 3){
    console.log("Opcion del Menu 3");
}else if(opt === 4){
    console.log("Opcion del Menu 4");
}else if(opt === 5){
    console.log("Opcion del Menu 5");
}else{
    console.log("Opcion del Menu Incorrecta");
}

switch(opt){
    case 1: 
        console.log("Opcion del Menu 1");
        break;
    case 2: 
        console.log("Opcion del Menu 2");
        break;
    case 3: 
        console.log("Opcion del Menu 3");
        break;
    case 4: 
        console.log("Opcion del Menu 4");
        break;
    case 5: 
        console.log("Opcion del Menu 4");
        break;
    default: 
        console.log("Opcion del Menu Incorrecta");
        break;
}