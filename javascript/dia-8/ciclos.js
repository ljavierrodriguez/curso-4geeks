// Ciclos While, do while, for, for anidados.

var n = 1;
while(n <= 10){
    console.log(n);
    n++;
} // Se va a mantener ejecutando mientras la condicion sea verdadera

n = 1;
do {
    console.log(n);
    n++;
}while(n<=10); 
// Se va a mantener ejecutando mientras la condicion sea verdadera, 
// y se ejecutara solo una vez.

// Ciclo for(inicializador; condicion; incremento)
for(var i = 1; i <= 10; i++){
    console.log(i);
}

var multi = [[1, 1], [10,10], [20, 20]];

for(var i = 0; i < multi.length; i++){
    for(var j=0; j < multi[i].length; j++){
        console.log(multi[i][j]);
    }
}